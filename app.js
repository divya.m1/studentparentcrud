const express = require('express');
const app = express();
//routes
 const parentRoute = require('./routes/parentr');
 const studentRoute = require('./routes/studentr');
//middleware
app.use(express.json());
app.use(express.urlencoded({extended:false}));
 //parents
app.use("/parents",parentRoute);
//students
app.use("/students",studentRoute);


module.exports = app;