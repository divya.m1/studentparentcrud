const models = require('../models');


// post parents details
function save(req,res){
    const post = {
        pname : req.body.pname,
        pphone : req.body.pphone,
        sid : req.body.sid,
        number : req.body.number
}
models.Parent.create(post).then(results =>{
    res.status(201).json({
        message :"parent post created successfully",
        posts :results
    });

}).catch(error =>{
    res.status(500).json({
        message : " parent post failed",
        error : error
    })
})


}

//show 

function show(req,res){
    const id = req.params.id;
    models.Parent.findByPk(id).then(result =>{
        if(result){
            res.status(201).json(result)
        }
        else{
            res.status(404).json({
                message :"file not found"
            })
        }
    }).catch(error =>{
        res.status(501).json({
            message : "ssomething went wrong",
            error : error
        }) ;
    });

}

function getdetail(req,res){
   models. Parent.findAll({
        include: [{
          model:models.Student,
          attributes : {
              include :['sname'],exclude :'saddress'
          }
        },]
      })
      .then(result => {
          res.status(201).json({
              result : result
          })
      })
      .catch(error =>{
          res.status(501).json({
              message:"something went wrong",
              error:error

          })
      })
    

}

//update parents
function update(req,res){
    const id = req.params.id;
   //const sid = 2
    const updatedpost = {
        pname : req.body.pname,
        pphone : req.body.pphone,
        sid : req.body.sid
    } 
    models.Parent.update(updatedpost ,{where : {id:id}}
        ).then(result =>{
        res.status(200).json({
            message :"updated successfully",
            result : result
        })

    }).catch(err=>{
        console.log("djbf"+sid);

        res.status(501).json({
            message:"failed to update",
            error : err
        })
    })

}

//delete
function destroy(req,res){
    const id = req.params.id;
    models.Parent.destroy({where :{id:id}}).then(result =>{
        res.status(200).json({
            message :"deleted successfully",
            
        })
    }).catch(error =>{
        res.status(501).json({
            message :"failed to delete",
            error : error
        })
    })
    
    

}











module.exports = {
    save : save,
    show :show,
    getdetail :getdetail,
    update : update,
    destroy :  destroy
}