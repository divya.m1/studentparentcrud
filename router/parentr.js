const express = require('express');
const parentController = require('../controllers/parent.controller');
const router = express.Router();

//parents details Post
router.post("/post-parentsDetails",parentController.save);
router.get("/show-parentsDetails/:id",parentController.show);
router.get("/getdetail",parentController.getdetail);
router.patch("/update-parentsdetails/:id",parentController.update);
router.delete("/delete-parentsDetails/:id",parentController.destroy);

module.exports = router;