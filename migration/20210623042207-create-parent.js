'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Parents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.NUMERIC
      },
      pname: {
        type: Sequelize.STRING,
        allowNull: false
      },
      pphone: {
        type: Sequelize.NUMERIC,
        allowNull: false
      },
      number: {
        type: Sequelize.NUMBER,
        allowNull: false
      },
     
     sid: {
        type : Sequelize.NUMERIC,
        allowNull: false,
        references: {
          model: "Students",
          key: "id"
        }
       

      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Parents');
  }
};