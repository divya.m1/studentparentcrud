'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    
    static associate(models) {
      Student.hasOne(models.Parent,{
        foreignKey : 'id'

      })
      
      
    }
  };
  Student.init({
    sname: DataTypes.STRING,
    saddress: DataTypes.STRING
  },
  
  {
    sequelize,
    modelName: 'Student',
  });
  return Student;
};