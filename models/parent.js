'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Parent extends Model {
   static associate(models) {
      Parent.belongsTo(models.Student,{
       foreignKey : 'sid'
        
      })
      
    }
  };
  Parent.init({
    pname: DataTypes.STRING,
    pphone: DataTypes.NUMERIC,
   sid : DataTypes.NUMERIC,
   number : DataTypes.INTEGER
  },
   {
    sequelize,
    modelName: 'Parent',
  });
  return Parent;
};